# Wake UP

A game made for [Ludum Dare 47](https://ldjam.com/events/ludum-dare/47)

Wake Up is a thriller about a confusing story by an unnamed character. You wake up in your apartment hungry and disoriented, and soon realize that you are trapped in a strange loop of dreams.

Your mission is to find out what's going on and how to get out of this endless nightmare.

## Commands

* AWSD - Character movement
* SPACE - Interactions
* Mouse - for some menu interactions

## Developers

* Ramon Venson
* Jeison Cleiton Pandini
