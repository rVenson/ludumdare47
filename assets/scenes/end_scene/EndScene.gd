extends "res://assets/scenes/main_scene/MainScene.gd"

func start():
	AudioManager.play_music('the_end')
	GameManager.player.set_movement(false)
	camera.target = null
	DialogueSystem.add_dialogue([
		"Don't close your eyes yet",
		"He's losing a lot of blood",
		"Continue checking the heartbeat!",
		"",
		"On this monday morning a 32-year-old man was found injured at his apartment",
		"There are no confirmed suspects",
		"",
		"(Thanks for playing our game)",
		"",
		"WAKE UP!",
	])
	DialogueSystem.next_dialogue()
	
	GameManager.ui.dialogue.set_process(false)

func next_dialogue():
	DialogueSystem.next_dialogue()

func end():
	GameManager.change_scene('menu')

func wake_up():
	AudioManager.play_effect('wake_up', GameManager.player.transform.origin)

func play_alarm():
	AudioManager.play_effect('alarm', GameManager.player.transform.origin)

func play_police():
	AudioManager.play_effect('police', $Map/DefaultMap/Objects/TV.transform.origin)

func play_heart():
	AudioManager.play_effect('heart', GameManager.player.transform.origin)
