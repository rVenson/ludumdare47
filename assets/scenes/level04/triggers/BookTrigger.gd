extends Area

export var position_path : NodePath
onready var target = get_node(position_path)
var is_active = false

func interact(player):
	print(player)
	if !DialogueSystem.is_running and !is_active:
		is_active = true
		player.set_movement(false)
		player.visible = false
		
		var camera = GameManager.scene.camera
		
		camera.target = target 
		camera.rotation_degrees = Vector3(0, 0, 0)
		
		DialogueSystem.add_dialogue(['OK. Maybe I should read a little to distract'])
		DialogueSystem.next_dialogue()
		
		yield(DialogueSystem, "dialogue_end")
		
		camera.target = player
		camera.rotation_degrees = Vector3(-45, 0, 0)
		player.set_movement(true)
		player.visible = true
		
		yield(get_tree().create_timer(2), "timeout")
		
		is_active = false
