extends Area

var is_active = false

func _ready():
	play_effect()

func play_effect():
	yield(get_tree().create_timer(5 + (randi() % 10)), "timeout")
	if !is_active:
		AudioManager.play_effect("ding_dong", global_transform.origin)
		play_effect()

func interact(player):
	if !DialogueSystem.is_running and !is_active:
		is_active = true
		player.set_movement(false)
		DialogueSystem.add_dialogue(['WAKE UP!'])
		DialogueSystem.next_dialogue()
		AudioManager.play_effect("wake_up", player.transform.origin)
		yield(get_tree().create_timer(2), "timeout")
		#yield(DialogueSystem, 'dialogue_end')
		GameManager.change_scene('level01')
