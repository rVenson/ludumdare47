extends Spatial

export var camera_path : NodePath
export var player_path : NodePath

var camera = null
var player = null

func _ready():
	GameManager.scene = self
	load_paths()
	load_nodes()
	start()

func start():
	pass
	
func load_paths():
	camera = get_node(camera_path)
	player = get_node(player_path)
	camera.current = true
	
func load_nodes():
	if player and camera:
		camera.target = player
