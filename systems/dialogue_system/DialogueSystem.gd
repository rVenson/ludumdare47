extends Node

signal dialogue_end
signal dialogue_next

var dialogue_queue = []
var dialogue_ui = null
var is_running = false

func add_dialogue(queue : Array):
	dialogue_queue = queue
	emit_signal("dialogue_end")

func next_dialogue():
	is_running = true
	var text = dialogue_queue.pop_front()
	if text:
		emit_signal("dialogue_next", text)
	else:
		is_running = false
		emit_signal("dialogue_end")

func is_empty():
	return dialogue_queue.empty()

func reset():
	dialogue_queue = []
	next_dialogue()
