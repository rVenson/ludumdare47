extends Area

var is_active = false
export var coffee : NodePath
export var alarmTrigger : NodePath

func interact(player):
	if !is_active and get_node(coffee).visible:
		if !DialogueSystem.is_running and !is_active:
			is_active = true
			player.set_movement(false)
			DialogueSystem.add_dialogue([
				'This cafe reminds me a lot of my family!',
				'Why is it even here?'
			])
			DialogueSystem.next_dialogue()
			yield(DialogueSystem, 'dialogue_end')
			player.set_movement(true)
			get_node(coffee).queue_free()
			get_node(alarmTrigger).activate()
