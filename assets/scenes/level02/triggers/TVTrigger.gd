extends Area

var is_active = false
export var tv : NodePath
export var note : NodePath

func interact(player):
	if !DialogueSystem.is_running and !is_active:
		is_active = true
		get_node(tv).power_on()
		player.set_movement(false)
		DialogueSystem.add_dialogue([
			'On this monday morning a 32-year-old man was found...',
			'The weather this monday will be sunny and...',
			'What did you do?',
			'What',
			'did',
			'you',
			'do',
			'WHAT',
			'WAK...'
		])
		DialogueSystem.next_dialogue()
		yield(DialogueSystem, 'dialogue_end')
		get_node(tv).power_off()
		get_node(note).show()
		player.set_movement(true)
		queue_free()
