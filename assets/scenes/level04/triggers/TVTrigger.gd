extends Area

var is_active = false
export var tv_path : NodePath

func interact(player):
	if !is_active:
		if !DialogueSystem.is_running:
			is_active = true
			player.set_movement(false)
			
			var tv = get_node(tv_path)
			tv.power_on()
			
			DialogueSystem.add_dialogue([
				"Hello, ladies and gentleman",
				"In today's show we will present tips for living a good life",
				"Always start the day by reading a good book",
				"...",
				"WA ...",
				"...",
				"Don't close your eyes yet",
				"He's losing a lot of blood",
				"Continue checking the heartbeat!",
				"...",
				"KE ...",
				"There are 5 numbers, the middle one is...",
				"...",
				"Look on the shelf, he hid the formula somewhere",
				"...",
				"And to close, a little about the events of the week",
				"Ludum Dare 47 was a success!",
				"UP",
				"....",
			])
			DialogueSystem.next_dialogue()
			
			yield(DialogueSystem, "dialogue_end")
			
			tv.power_off()
			player.set_movement(true)
			
			yield(get_tree().create_timer(2), "timeout")
			
			is_active = false
