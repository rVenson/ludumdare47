extends KinematicBody

export var speed = 7
var movement = Vector3()
var can_move = true
var is_interacting = false
export var dead = false

func _ready():
	GameManager.player = self
	if dead:
		set_process(false)
		set_physics_process(false)
		$man/AnimationPlayer.play("Character|Act_Die")

func _physics_process(delta):
	apply_animation()
	apply_movement(delta)

func _process(delta):
	if Input.is_action_pressed("up"):
		movement.z = -1
	if Input.is_action_pressed("down"):
		movement.z = 1
	if Input.is_action_pressed("left"):
		movement.x = -1
	if Input.is_action_pressed("right"):
		movement.x = 1

	if Input.is_action_just_pressed("interact"):
		interact()

func apply_movement(delta):
	if can_move:
		move_and_collide(movement.normalized() * speed * delta)
		movement = Vector3()

func set_movement(state : bool):
	set_process(state)
	can_move = state
	movement = Vector3()

func interact():
	if !is_interacting:
		is_interacting = true
		var objects_in_area = $InteractableArea.get_overlapping_areas()
		if objects_in_area:
			var first_object : Node = objects_in_area[0]
			
			if first_object.has_method("interact"):
				first_object.interact(self)
		
		yield(get_tree().create_timer(1), "timeout")
		is_interacting = false

func apply_animation():
	if movement.length() > 0:
		look_at(transform.origin + movement, Vector3(0, 1, 0))
	
	if movement.length() > 0:
		$man/AnimationPlayer.play("Character|Act_Walk")
	else:
		$man/AnimationPlayer.play("Character|Act_Normal_Idle")
