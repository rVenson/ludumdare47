extends Control

var unlocked = false
onready var actual_screen = $Code
var last_screen = null

signal phone_exit
signal wake_up

func open():
	if unlocked:
		change_screen($MainMenu)
	else:
		change_screen($Code)
		
	show()

func change_screen(new_screen):
	actual_screen.hide()
	last_screen = actual_screen
	actual_screen = new_screen
	actual_screen.show()

func last_screen():
	if last_screen:
		change_screen(last_screen)
		if actual_screen == $MainMenu:
			last_screen = null

func _on_IconGames_pressed():
	change_screen($Games)

func _on_IconSMS_pressed():
	change_screen($SMS)

func _on_IconCall_pressed():
	change_screen($Calls)

func _on_ButtonOK_pressed():
	change_screen($MainMenu)

func _on_BackButton_pressed():
	last_screen()

func _on_HomeButton_pressed():
	last_screen = null
	change_screen($MainMenu)

func _on_Code_unlocked():
	change_screen($MainMenu)

func _on_Code_exit():
	emit_signal("phone_exit")
	hide()

func _on_SMS_wake_up():
	emit_signal("wake_up")
