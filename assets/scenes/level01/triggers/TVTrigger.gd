extends Area

var is_active = false
export var tv_path : NodePath

func interact(player):
	if !is_active:
		if !DialogueSystem.is_running:
			is_active = true
			player.set_movement(false)
			
			var tv = get_node(tv_path)
			tv.power_on()
			
			DialogueSystem.add_dialogue([
				"Hello, ladies and gentleman",
				"Since the start of the pandemic, no one has left home",
				"So we decided...",
				"...",
				"Give me the formula or you will die",
				"..."
			])
			DialogueSystem.next_dialogue()
			
			yield(DialogueSystem, "dialogue_end")
			
			tv.power_off()
			player.set_movement(true)
			
			yield(get_tree().create_timer(2), "timeout")
			
			is_active = false
