extends Node

signal scene_loaded

var game = null
var player = null
var ui = null
var scene = null
var transitions = null

var resource_path = {
	"scenes": {
		"menu": "res://assets/ui/main_menu/MainMenu.tscn",
		"main": "res://assets/scenes/main_scene/MainScene.tscn",
		"intro": "res://assets/scenes/intro_scene/IntroScene.tscn",
		"level01": "res://assets/scenes/level01/Level01.tscn",
		"level02": "res://assets/scenes/level02/Level02.tscn",
		"level03": "res://assets/scenes/level03/Level03.tscn",
		"level04": "res://assets/scenes/level04/Level04.tscn",
		"end": "res://assets/scenes/end_scene/EndScene.tscn",
	},
	"musics": {
		"track000": "res://audio/music/track000.ogg",
		"the_end": "res://audio/music/the_end.ogg"
	},
	"effects": {
		"ding_dong": "res://audio/fx/ding_dong.wav",
		"wake_up": "res://audio/fx/wake_up.wav",
		"alarm": "res://audio/fx/alarm.wav",
		"police": "res://audio/fx/police.wav",
		"heart": "res://audio/fx/heart.wav",
		"interference": "res://audio/fx/alarm.wav",
	}
}

var loaded_resources = {
	"scenes": {},
	"musics": {},
	"effects": {}
}

func mem_load():
	for scene in resource_path.scenes:
		loaded_resources["scenes"][scene] = load(resource_path.scenes[scene])
		
	for music in resource_path.musics:
		loaded_resources["musics"][music] = load(resource_path.musics[music])
	
	for effect in resource_path.effects:
		loaded_resources["effects"][effect] = load(resource_path.effects[effect])

func _ready():
	mem_load()
	pause_mode = Node.PAUSE_MODE_PROCESS

func game_over():
	get_tree().paused = true
	ui.game_over.start_animation()

func restart():
	load_scene('menu')
	
func load_scene(scene_name):
	if game:
		if scene:
			scene.queue_free()
		scene = loaded_resources.scenes[scene_name].instance()
		game.add_child(scene)
		game.move_child(scene, 0)

func change_scene(scene_name):
	get_tree().paused = true
	if transitions:
		yield(transitions.start_transition('fadein'), "animation_finished")
	load_scene(scene_name)
	if transitions:
		transitions.start_transition('fadeout')
	emit_signal("scene_loaded")
	get_tree().paused = false
