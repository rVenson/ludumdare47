extends Control

func _on_ButtonStart_pressed():
	$AnimationPlayer.play("start")
	GameManager.change_scene('intro')
	yield(GameManager, "scene_loaded")
	visible = false
	AudioManager.play_music("track000")

func _on_ButtonExit_pressed():
	get_tree().quit()

func _on_ButtonCredits_pressed():
	$Credits.show()

func _on_ButtonBack_pressed():
	$Credits.hide()
