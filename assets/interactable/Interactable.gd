extends Area

export var object_name = "Interactable"

func interact(player):
	print(player.name + " interacts with " + object_name)
