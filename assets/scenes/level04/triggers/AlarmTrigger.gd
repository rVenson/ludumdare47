extends Area

var is_active = false
export var alarm_screen : NodePath

func interact(player):
	if !is_active:
		if !DialogueSystem.is_running and !is_active:
			is_active = true
			player.set_movement(false)
			
			var alarm = get_node(alarm_screen)
			alarm.show()
			yield(alarm, 'finished')
			
			player.set_movement(true)
			is_active = false
