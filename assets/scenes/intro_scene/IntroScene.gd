extends "res://assets/scenes/main_scene/MainScene.gd"

func start():
	player.set_movement(false)
	DialogueSystem.add_dialogue([
		'My stomach hurts...',
		'Maybe I need some food'
	])
	DialogueSystem.next_dialogue()
	yield(DialogueSystem, "dialogue_end")
	player.set_movement(true)
