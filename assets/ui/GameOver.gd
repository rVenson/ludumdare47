extends Control

func _ready():
	set_process(false)

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		GameManager.restart()

func start_animation():
	$Box/AnimationPlayer.play("default")
	set_process(true)
