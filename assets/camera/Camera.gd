extends Camera

export var speed = 10
var target = null
var padding = Vector3(0, 10, 10)

func _physics_process(delta):
	if target:
		var offset = target.transform.origin - transform.origin
		transform.origin += (offset + padding) * delta * speed
