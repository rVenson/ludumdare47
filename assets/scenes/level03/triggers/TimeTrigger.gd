extends Area

export var blood_path : NodePath
var blood = null

func _process(delta):
	if blood:
		blood.scale.x += delta * 0.08
		blood.scale.z += delta * 0.02

func _ready():
	var player = GameManager.player
	
	yield(get_tree().create_timer(15), "timeout")
	
	player.set_movement(false)
	DialogueSystem.add_dialogue([
		'What am I doing here?',
		'Why am I here?'
	])
	DialogueSystem.next_dialogue()
	yield(DialogueSystem, 'dialogue_end')
	player.set_movement(true)
	
	yield(get_tree().create_timer(10), "timeout")
	
	player.set_movement(false)
	DialogueSystem.add_dialogue([
		'I need to wake...'
	])
	DialogueSystem.next_dialogue()
	yield(DialogueSystem, 'dialogue_end')
	player.set_movement(true)
	
	blood = get_node(blood_path)
	
	yield(get_tree().create_timer(7), "timeout")
	
	player.set_movement(false)
	DialogueSystem.add_dialogue([
		'I\'m feeling really bad right now'
	])
	DialogueSystem.next_dialogue()
	yield(DialogueSystem, 'dialogue_end')
	
	yield(get_tree().create_timer(2), "timeout")
	
	DialogueSystem.add_dialogue([
		'WAKE UP!'
	])
	DialogueSystem.next_dialogue()
	
	AudioManager.play_effect("wake_up", player.transform.origin)
	
	yield(get_tree().create_timer(2), "timeout")
	
	GameManager.change_scene('level04')
