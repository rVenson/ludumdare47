extends Area

export var audio_name = ""
export var dialogue = []
var is_active = false

func interact(player):
	if !DialogueSystem.is_running and !is_active:
		is_active = true
		if audio_name:
			AudioManager.play_effect(audio_name, global_transform.origin)
		player.set_movement(false)
		DialogueSystem.add_dialogue(dialogue.duplicate())
		DialogueSystem.next_dialogue()
		yield(DialogueSystem, 'dialogue_end')
		player.set_movement(true)
		yield(get_tree().create_timer(1), "timeout")
		is_active = false
