extends "res://assets/scenes/main_scene/MainScene.gd"

func start():
	player.set_movement(false)
	DialogueSystem.add_dialogue([
		'What happened?',
		'Oh shit, I was dreaming',
		"My stomach still hurts"
	])
	DialogueSystem.next_dialogue()
	yield(DialogueSystem, "dialogue_end")
	player.set_movement(true)
