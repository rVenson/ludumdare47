extends "res://assets/scenes/main_scene/MainScene.gd"

func start():
	GameManager.player.set_movement(true)

func _on_SmarthphoneScreen_wake_up():
	DialogueSystem.add_dialogue(['WAKE UP!'])
	DialogueSystem.next_dialogue()
	AudioManager.play_effect('wake_up', GameManager.player.transform.origin)
	yield(get_tree().create_timer(2), "timeout")
	GameManager.change_scene('end')
