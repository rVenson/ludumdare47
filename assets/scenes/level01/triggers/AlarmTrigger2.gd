extends Area

var is_started = false
var is_active = false

var audio_player = null

func activate():
	audio_player = AudioManager.play_effect("alarm", global_transform.origin, true)
	is_started = true

func interact(player):
	if !DialogueSystem.is_running and !is_active and is_started:
		is_active = true
		player.set_movement(false)
		DialogueSystem.add_dialogue([
			'What\'s this?',
			"I can't shutdown this damn alarm"
		])
		DialogueSystem.next_dialogue()
		yield(DialogueSystem, 'dialogue_end')
		
		yield(get_tree().create_timer(2), "timeout")
		DialogueSystem.add_dialogue([
			'WAKE UP!'
		])
		DialogueSystem.next_dialogue()
		AudioManager.play_effect('wake_up', player.global_transform.origin)
		get_tree().paused = true
		
		yield(get_tree().create_timer(2), "timeout")
		audio_player.queue_free()
		GameManager.change_scene('level02')
