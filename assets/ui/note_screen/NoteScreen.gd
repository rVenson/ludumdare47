extends Control

signal note_hide
signal note_show

func set_note(text):
	$Text.text = text
	
func show():
	visible = true
	emit_signal("note_show")
	
func hide():
	visible = false
	emit_signal('note_hide')

func _on_CloseButton_pressed():
	hide()
