extends Control

func show_message(msg):
	$Title.text = msg.title
	$Desc.text = msg.desc
	show()
