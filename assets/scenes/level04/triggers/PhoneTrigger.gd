extends Area

var is_active = false
export var phone : NodePath
export var phone_screen : NodePath

func interact(player):
	if !is_active and get_node(phone).visible:
		if !DialogueSystem.is_running and !is_active:
			is_active = true
			player.set_movement(false)
			
			var phone = get_node(phone_screen)
			phone.open()
			yield(phone, 'phone_exit')
			
			player.set_movement(true)
			is_active = false
