extends Control

func _process(delta):
	if Input.is_action_just_pressed("pause"):
		set_pause(!get_tree().paused)

func set_pause(status):
	get_tree().paused = status
	$Background.visible = status
	$main.visible = status
	$confirm_escape.visible = false

func _on_ButtonGiveUp_pressed():
	$confirm_escape.visible = true
	$main.visible = false

func _on_ButtonNo_pressed():
	$confirm_escape.visible = false
	$main.visible = true

func _on_ButtonYes_pressed():
	GameManager.change_scene('menu')
