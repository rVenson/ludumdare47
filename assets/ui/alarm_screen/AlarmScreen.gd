extends Control

signal cancel
signal confirm
signal finished

func _on_up_hour_pressed():
	var new_number = int($Panel/selectors/hour.text) + 1
	
	if new_number > 12:
		new_number = 0
	
	$Panel/selectors/hour.text = str(new_number)

func _on_up_minutes_pressed():
	var new_number = int($Panel/selectors/minutes.text) + 1
	
	if new_number > 59:
		new_number = 0
	
	$Panel/selectors/minutes.text = str(new_number)

func _on_change_ampm_pressed():
	if $Panel/selectors/ampm.text == "PM":
		$Panel/selectors/ampm.text = "AM"
	else:
		$Panel/selectors/ampm.text = "PM"

func _on_down_minutes_pressed():
	var new_number = int($Panel/selectors/minutes.text) - 1
	
	if new_number < 0:
		new_number = 59
	
	$Panel/selectors/minutes.text = str(new_number)

func _on_down_hour_pressed():
	var new_number = int($Panel/selectors/hour.text) - 1
	
	if new_number < 0:
		new_number = 12
	
	$Panel/selectors/hour.text = str(new_number)

func get_configured_alarm():
	return $Panel/selectors/hour.text + ":" + $Panel/selectors/minutes.text + $Panel/selectors/ampm.text

func _on_ButtonConfirm_pressed():
	var hour_code = get_configured_alarm()
	hide()
	print(hour_code)
	emit_signal("confirm", hour_code)
	emit_signal("finished")

func _on_ButtonCancel_pressed():
	hide()
	emit_signal("finished")
