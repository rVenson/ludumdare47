extends Area

var is_active = false
export var coffee : NodePath

var alarm_audio = null

func _ready():
	alarm_audio = AudioManager.play_effect("alarm", global_transform.origin, true)

func interact(player):
	if !DialogueSystem.is_running and !is_active:
		is_active = true
		player.set_movement(false)
		DialogueSystem.add_dialogue([
			'Damn alarm clock!'
		])
		DialogueSystem.next_dialogue()
		alarm_audio.queue_free()
		yield(DialogueSystem, 'dialogue_end')
		player.set_movement(true)
		get_node(coffee).show()
		queue_free()
