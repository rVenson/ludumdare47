extends Node

var audio_player : AudioStreamPlayer = null

func _ready():
	pause_mode = Node.PAUSE_MODE_PROCESS
	load_music_player()

func load_music_player():
	audio_player = AudioStreamPlayer.new()
	add_child(audio_player)

func load_effect_player():
	var fx_player = AudioStreamPlayer3D.new()
	add_child(fx_player)
	return fx_player

func play_music(name : String):
	audio_player.stream = GameManager.loaded_resources.musics[name]
	audio_player.play()

func play_effect(name : String, position, loop = false):
	var fx_player : AudioStreamPlayer3D = load_effect_player()
	fx_player.stream = GameManager.loaded_resources.effects[name]
	fx_player.transform.origin = position
	fx_player.unit_db = 18
	fx_player.max_db = 60
	fx_player.play()
	if loop:
		return fx_player
	else:
		yield(fx_player, "finished")
		fx_player.queue_free()
