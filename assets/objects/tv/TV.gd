extends Spatial

func power_on():
	$TV.mesh.surface_set_material(1, preload("res://assets/objects/tv/TVon.material"))
	
func power_off():
	$TV.mesh.surface_set_material(1, preload("res://assets/objects/tv/TVoff.material"))
