extends "res://assets/scenes/main_scene/MainScene.gd"

func start():
	player.set_movement(false)
	DialogueSystem.add_dialogue([
		'...',
		'Now even my head hurts',
		"I hope that this is not another bad dream"
	])
	DialogueSystem.next_dialogue()
	yield(DialogueSystem, "dialogue_end")
	player.set_movement(true)
