extends Area

var is_active = false
export var note : NodePath
export var note_screen : NodePath

func interact(player):
	if !is_active and get_node(note).visible:
		if !DialogueSystem.is_running and !is_active:
			is_active = true
			player.set_movement(false)
			
			var note = get_node(note_screen)
			note.set_note("I have a date tomorrow at 7PM. I need to remember to leave the house at least half an hour before.")
			note.show()
			yield(note, 'note_hide')
			
			player.set_movement(true)
			is_active = false
