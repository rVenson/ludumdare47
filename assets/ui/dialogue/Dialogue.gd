extends Control

func _ready():
	DialogueSystem.dialogue_ui = self
	DialogueSystem.connect("dialogue_next", self, "dialogue_next")
	DialogueSystem.connect("dialogue_end", self, "dialogue_end")

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		DialogueSystem.next_dialogue()

func dialogue_end():
	set_process(false)
	$AnimationPlayer.play("fadeout")

func dialogue_next(text):
	set_process(false)
	show_message(text)
	yield(get_tree().create_timer(1), "timeout")
	set_process(true)

func show_message(text):
	$Box/Label.text = text
	if !$Box.visible:
		$AnimationPlayer.play("fadein")
