extends Area

var is_active = false
export var book : NodePath
export var book_screen : NodePath

func interact(player):
	if !is_active and get_node(book).visible:
		if !DialogueSystem.is_running and !is_active:
			is_active = true
			player.set_movement(false)
			
			var book = get_node(book_screen)
			book.show()
			yield(book, 'book_hide')
			
			player.set_movement(true)
			is_active = false
