extends Area

var is_active = false
export var alarm_path : NodePath

func interact(player):
	if !DialogueSystem.is_running and !is_active:
		is_active = true
		player.set_movement(false)
		
		var alarm_screen = get_node(alarm_path)
		alarm_screen.show()
		yield(alarm_screen, 'finished')
		
		if alarm_screen.get_configured_alarm() == "6:30PM":
			AudioManager.play_effect('wake_up', player.transform.origin)
			yield(get_tree().create_timer(2), "timeout")
			GameManager.change_scene('level03')
		else:
			player.set_movement(true)
			is_active = false
