extends Control

signal book_hide
signal book_show

func set_book(text):
	$Text.text = text
	
func show():
	visible = true
	emit_signal("book_show")
	
func hide():
	visible = false
	emit_signal('book_hide')

func _on_Button_pressed():
	hide()
