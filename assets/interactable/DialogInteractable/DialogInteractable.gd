extends "res://assets/interactable/Interactable.gd"

export var dialogue_sequence = []

func interact(player):
	DialogueSystem.add_dialogue(dialogue_sequence)
	DialogueSystem.next_dialogue()
	GameManager.change_scene("level01")
