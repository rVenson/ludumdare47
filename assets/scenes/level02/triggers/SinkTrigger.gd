extends Area

var is_active = false
export var tv : NodePath

func interact(player):
	if !DialogueSystem.is_running and !is_active:
		is_active = true
		player.set_movement(false)
		DialogueSystem.add_dialogue([
			'The sink is dripping',
			'Is this water?'
		])
		DialogueSystem.next_dialogue()
		yield(DialogueSystem, 'dialogue_end')
		player.set_movement(true)
		get_node(tv).power_on()
		queue_free()
