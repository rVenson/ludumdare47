extends Control

func _ready():
	GameManager.transitions = self

func start_transition(name):
	$AnimationPlayer.play(name)
	return $AnimationPlayer
