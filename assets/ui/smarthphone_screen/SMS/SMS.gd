extends Control

signal wake_up

func _on_msg001_pressed():
	$Messages.show_message({
		"title": "unknown",
		"desc": "WAKE UP"
	})
	emit_signal("wake_up")

func _on_msg002_pressed():
	$Messages.show_message({
		"title": "Jimmy",
		"desc": "Hey man! Things are getting weird. Last night a guy came to me wanting to know about the formula you developed. Didn't you say it was kind of secret? I didn't really say anything about it, but I'm afraid we're in trouble"
	})

func _on_msg003_pressed():
	$Messages.show_message({
		"title": "Gloria",
		"desc": "We need to talk about the last night. Why did you leave like that? I thought we were fine, but it looks like you're still the same person."
	})

func _on_SMS_hide():
	$Messages.hide()
