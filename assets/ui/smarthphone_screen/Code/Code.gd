extends Control

signal unlocked
signal exit
var code = "84347"

func _on_Button_pressed(button):
	$TextEdit.text += button

func _on_ButtonClear_pressed():
	$TextEdit.text = ""

func _on_ButtonOK_pressed():
	if $TextEdit.text == code:
		emit_signal("unlocked")

func _on_ButtonExit_pressed():
	emit_signal("exit")
